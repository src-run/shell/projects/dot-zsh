#!/usr/bin/env zsh

#
# This file is part of the `src-run/dot-zsh` project.
#
# (c) Rob Frawley 2nd <rmf@src.run>
#
# For the full copyright and license information, view the LICENSE.md
# file distributed with this source code.
#


#
# Root dir path and loader filepath.
#

D_ZSH_ROOT_PATH="${HOME}/.dot-zsh"
D_ZSH_LOAD_FILE="$D_ZSH_ROOT_PATH/load.zsh"


#
# Dir path of available/enabled configuration paths.
#

D_ZSH_CFG_AVAIL_PATH="$D_ZSH_ROOT_PATH/confs-available"
D_ZSH_CFG_ENABL_PATH="$D_ZSH_ROOT_PATH/confs-enabled"


#
# Dir path of avail/enable include paths.
#

D_ZSH_INC_AVAIL_PATH="$D_ZSH_ROOT_PATH/incs-available"
D_ZSH_INC_ENABL_PATH="$D_ZSH_ROOT_PATH/incs-enabled"


# EOF
